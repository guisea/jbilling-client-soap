# Object Representation of UserWS
package jBilling::Client::SOAP::UserWS;
    our $VERSION = 0.01;
use Data::Dumper;
use strict;

#use warnings FATAL => 'all';

my $id;     # Unique identifier for this record
my $ach;    # ACH Payment details saved for the User
my $autoRecharge
  ;    # Amount by which the customers account is auto-recharged when depleted
my $automaticPaymentType
  ; # Integer Value to determine which of the three payment methods does the customer want to apply for automatic payment processing
my $balanceType
  ; # The type of dynamic balance for this user. Refer to Appendix A for acceptable values.
my $blacklistMatches;    # Lists any blacklist matches for this user.
my $childIds;            # The identifiers of any sub-accounts for this user.
my $companyName;         # The Users company name.
my $contact;             # ContactWS Object - Primary contact info for customer
my $createDatetime;      # Creation Date of this data record
my $creditCard;          # CreditCardDTO Object
my $creditLimit
  ;    # The Credit Limit. Only valid if balanceType is of credit limit type
my $currencyId;       # Contains the currency code for this user
my $customerId;       # Reference to the Customer information for this User
my $deleted;          # If the record has been deleted
my $dueDateUnitId;    # Period Unit of this customer's invoice due date
my $dueDateValue;     # Customer specific Invoice Due Date Value
my $dynamicBalance;   # String representation of this Customers dynamic balance.
my $excludeAgeing
  ; # Boolean value to indicate excluding this User/Customer from the Ageing process
my $failedAttempts
  ;    # Number of login attempts that have been failed by this user
my $invoiceChild
  ; # true if this is a sub-account (child of a parent account), but this user will still receive invoices.
my $invoiceDeliveryMethodId
  ;    # Reference ID for one of the Invoice Delivery Methods
my $isParent;            # true if this record is a "parent" user.
my $language;            # Name of the language (i.e English)
my $languageId;          # Contains the preferred language code for the user
my $lastLogin;           # Date of the last login performed by this user
my $lastStatusChange;    # Date of the last status change for this user
my $mainRoleId
  ;    # The level of privelege granted tot he user when logged into the system
my $nextInvoiceDate;    # The earliest next billable date for this

# Create an actions list for populating the object with data
my %actions;

=head1 SUBROUTINES/METHODS

=cut

=head2 new

Creates a new instance of the class

=cut

sub new {
    my $class = shift;
    my $self  = {};
    bless( $self, $class );
    $self->setActions();
    return $self;
}

sub setActions {
    my $self = shift;
    $self->{actions} = {
        'userId'       => \&setId,
        'autoRecharge' => \&setautoRecharge
    };
}

=head2 Id

Returns the UserId of this customer in this object.

=cut

sub Id {
    my $self = shift;
    return $self->{'_id'};
}

=head2 setId

Sets the UserId of this customer in this object.

=cut

sub setId {
    my $self = shift;
    my $id   = shift;
    $self->{_id} = $id;
}

=head2 autoRecharge

Returns the autoRecharge value in this object.

=cut

sub autoRecharge {
    my $self = shift;
    return $self->{'autoRecharge'};
}

=head2 setautoRecharge

Sets the autoRecharge of this customer in this object.

=cut

sub setautoRecharge {
    my $self = shift;
    my $id   = shift;
    $self->{autoRecharge} = $id;
}

=head2 setCompanyName

Sets the Users Company Name in this object.

=cut

sub setcompanyName {
    my $self = shift;
    my $name = shift;
    $self->{'companyName'} = $name;
}

=head2 companyName 

Returns the companyName value

=cut

sub companyName {
    my $self = shift;
    return $self->{'companyName'};
}

=head2 populate 

This method will populate the class from returned SOAP Data

=cut

sub populate {
    my $self     = shift;
    my $response = shift;
    my @keys     = keys %{ $self->{'actions'} };
    my %hash     = %{ $self->{'actions'} };
    while ( ( my $key, my $value ) = each(%$response) ) {
        if ( $key ~~ @keys ) {
            $self ->${ \$self->{actions}{$key} }($value);
        }
    }

}


=head2 function1

=cut

sub function1 {
}

=head2 function2

=cut

sub function2 {
}

=head1 AUTHOR

Aaron Guise, C<< <aaron at guise.net.nz> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-jbilling-client-soap at rt.cpan.org>, or through
the web interface at L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=jBilling-Client-SOAP>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.




=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc jBilling::Client::SOAP


You can also look for information at:

=over 4

=item * RT: CPAN's request tracker (report bugs here)

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=jBilling-Client-SOAP>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/jBilling-Client-SOAP>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/jBilling-Client-SOAP>

=item * Search CPAN

L<http://search.cpan.org/dist/jBilling-Client-SOAP/>

=back


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

Copyright 2014 Aaron Guise.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

L<http://www.perlfoundation.org/artistic_license_2_0>

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, tradename, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


=cut

1;    # End of jBilling::Client::SOAP::UserWS
