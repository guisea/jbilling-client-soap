use 5.010;
use jBilling::Client::SOAP;
use Try::Tiny;
use Data::Dumper;

try {
    my $jb = jBilling::Client::SOAP->new();
    $jb->getAPI(
        'username' => 'example;10', # in format 'username;entity_id'
        'password' => 'iamapassword', # Self explanatory :)
        'url'      => 'http://jbilling-server-address:8080/jbilling/services/api' # This is the full url to the jbilling API
    );    # Initialise the API
    
    say ref($jb);
    my $order = jBilling::Client::SOAP::OrderWS->new(
        'jbilling' => $jb,     # The Client connection to jBilling
        'id'       => 25300    # Specify the ID which we want to load
    )->retrieve;               # Retrieve the order details from jBilling

    say "Order ID "
      . $order->id
      . " has a billingType of "
      . $order->billingTypeStr;
    say '';
    say "The entire order object follows: ";
    say Dumper($order);
    say '';
    say "Metafield data line by line";

    # Want to get data from metaFields or orderLines?
    foreach my $line ( @{ $order->metaFields } ) {
        say 'FieldName '
          . $line->fieldName
          . ' has a value of '
          . $line->stringValue;
    }

    say '';
    say "OrderLines data line by line";
    foreach my $line ( @{ $order->orderLines } ) {
        say 'OrderLine '
          . $line->id
          . ' has a description of '
          . $line->description;
    }
}
catch {
    warn "Caught error: $_";
};

