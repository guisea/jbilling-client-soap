use 5.010;
use lib '../lib';
use jBilling::Client::SOAP;
use Try::Tiny;
use Data::Dumper;

my $jb = undef;
try {
    $jb = jBilling::Client::SOAP->new();
    $jb->getAPI(
        'username' => 'example;10', # in format 'username;entity_id'
        'password' => 'iamapassword', # Self explanatory :)
        'url'      => 'http://jbilling-server-address:8080/jbilling/services/api' # This is the full url to the jbilling API
    );    # Initialise the API
    
    
}
catch {
    warn "Caught error: $_";
};

my %itemLookup = ('Recurring' => '303',
                  'Transactional' => '302',
                  'Ancillary' => '304');
my %superhash;
while (my ($key, $value) = each %itemLookup) {
    my @response = $jb->getItemByCategory($value);
    my %items;
    while (@response){
        $item = shift @response;
        $items{$item->description} = $item->id;
        #say "Id is " . $item->id . "\t" . "Description is: " . $item->description;
        #say "Actual object is: " . Dumper($item);
        
    }
    $superhash{$key} = \%items;
    
}

my $priceBook = \%superhash;

say Dumper($priceBook);
#while (my ($key, $value) = each $priceBook->{'Recurring'} ) {
#    say "Id: $value \t Name: $key";
#}


#while (my %item = @response)
#{
#  # do whatever you want with $key and $value here ..
#  say "Item Contains";
#  say Dumper(%item);
#  
#}