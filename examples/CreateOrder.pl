use 5.010;
use jBilling::Client::SOAP;
use Try::Tiny;
use Data::Dumper;

my $jb = undef;
try {
    $jb = jBilling::Client::SOAP->new();
    $jb->getAPI(
        'username' => 'example;10', # in format 'username;entity_id'
        'password' => 'iamapassword', # Self explanatory :)
        'url'      => 'http://jbilling-server-address:8080/jbilling/services/api' # This is the full url to the jbilling API
    );    # Initialise the API
    
    
}
catch {
    warn "Caught error: $_";
};
    my $ol = jBilling::Client::SOAP::OrderLineWS->new('useItem' => 'true',
                                                      'itemId' => 2103, # MAKE SURE THIS ITEM MATCHES AN ITEM YOUR SYSTEM!
                                                      'typeId' => 1, # Item
                                                      'quantity' =>  1,
                                                      'description' => "test from perl api",
                                                      );
    my $ol1 = jBilling::Client::SOAP::OrderLineWS->new('useItem'    => 'true',
                                                       'itemId'     => '2102', # MAKE SURE THIS ITEM MATCHES AN ITEM YOUR SYSTEM!
                                                       'typeId'     => 1, # Item
                                                       'quantity'   => 1,
                                                       'description' => "Another test from perl api" );

        my $retailer = jBilling::Client::SOAP::MetaFieldValueWS->new(
            'fieldName'   => 'retailer',
            'stringValue' => 'Orcon',
        );
        my $orderno = jBilling::Client::SOAP::MetaFieldValueWS->new(
            'fieldName'   => 'Order Number',
            'stringValue' => 'TestOrderNo'
        );

        my $piid = jBilling::Client::SOAP::MetaFieldValueWS->new(
            'fieldName'   => 'Product Instance ID',
            'stringValue' => 'TestPIID'
        );
        my $segment = jBilling::Client::SOAP::MetaFieldValueWS->new(
            'fieldName'   => 'Customer Segments',
            'stringValue' => 'Residential'
        );

        my $cabinet = jBilling::Client::SOAP::MetaFieldValueWS->new(
            'fieldName'   => 'Cabinet ID',
            'stringValue' => 'Test Cabinet'
        );

        my $candidatearea = jBilling::Client::SOAP::MetaFieldValueWS->new(
            'fieldName'   => 'Candidate Area',
            'stringValue' => 'Hamilton (HW)'
        );

        my @metafields =
          ( $retailer, $orderno, $piid, $segment, $cabinet, $candidatearea );
        my @OrderLines = ($ol, $ol1);
        
        say ref($jb);
        # create new order
        my $newOrder = jBilling::Client::SOAP::OrderWS->new('jbilling' => $jb,
                                                            'period' => '200',
                                                            'billingTypeId' => '2',
                                                            'currencyId' => '110',
                                                            'userId' => '122',
                                                            'activeSince' => '2015-01-31',
                                                            'notes' => 'Some Order notes from the API');
        # add our order lines to the order
        $newOrder->orderLines(\@OrderLines);
        $newOrder->metaFields(\@metafields);
        #
        say "Order Details Created";
        #say Dumper($newOrder);
        #
        say 'Try create Order';
        my $res = $newOrder->save;
        say Dumper($res);
        say "jBilling returned new order id " .$res if defined $res;
        say "Order Details Returned: ";
        $newOrder->id($res);
        $newOrder->retrieve;
        say Dumper($newOrder);
        


